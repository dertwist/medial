medial
======

Abstract
--------

`medial` is a commandline client for ardmediathek


Setup:
------

`medial` is almost self-contained and uses only standard ruby libraries + nokogiri. if `ruby --version` is 
successful and a `gem list --local` lists nokogiri  your good to go.
otherwise install ruby and rubygems through your prefered channel


copy `medial` in your `$HOME/bin` directory or run it localy with `./medial`

Usage
-----


     medial [options] [URL1..N]
    
### Options ###

     -x, --execute=DOWNLOADER         use DOWNLOADER (wget, curl, ...) to download stream
                                      provide custom options within the DOWNLOADER string.

     -q, --quality={0|1|2|3}          select the quality to download, default is 2

     -i, --input=FILE                 read the urls from FILE (separated by newline)

     -l, --list={LETTER}              list all available streams for LETTER

     --search=PATTERN                 search all available streams for PATTERN

     -d, --[no-]debug                 output debug information


### Default behaviour ###

If there are no options given medial will simply print the stream urls which can be 
copy and pasted or redirected into other programs to download the videos.
All urls will be for quality 2.

### Examples ###

    # Download the video using wget and custom wget options"
    medial --execute "wget --limit-rate=100k" http://www.example.com/

    # Download the video using wget and the best quality
    medial --execute "wget" --quality=3 http://www.example.com/

    # use medial as argument deliverer
    wget `medial http://www.example.com/`



Disclaimer
----------

i have **no** idea about the legality of downloading videos from ardmediathek.
`medial` is a proof of concept. use at your own risk.

